﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CounterLibrary;

namespace BasicCounterApp
{
    public partial class Form1 : Form
    {
        Counter compteur = new Counter();
        public Form1()
        {
            InitializeComponent();
        }

    

        private void decrease_button_Click(object sender, EventArgs e)
        {
            compteur.decreaseTotal();
            total.Text = string.Format("{0}", compteur.getTotal());
        }

        private void increase_button_Click(object sender, EventArgs e)
        {
            compteur.increaseTotal();
            total.Text = string.Format("{0}", compteur.getTotal());
        }

        private void reset_button_Click(object sender, EventArgs e)
        {
            compteur.resetTotal();
            total.Text = string.Format("{0}", compteur.getTotal());
        }
    }
}
