﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CounterLibrary;

namespace BasicCounterTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestIncrease()
        {
            Counter compteur = new Counter();
            compteur.increaseTotal();
            Assert.AreEqual(1, compteur.getTotal());
        }
        public void TestDecrease()
        {
            Counter compteur = new Counter();
            compteur.setTotal(8);
            compteur.decreaseTotal();
            Assert.AreEqual(7, compteur.getTotal());
        }
        public void TestReset()
        {
            Counter compteur = new Counter();
            compteur.setTotal(8);
            compteur.resetTotal();
            Assert.AreEqual(0, compteur.getTotal());
        }
    }
}
