﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterLibrary
{
    public class Counter
    {
        private int total;
        public Counter()
        {
            this.total = 0;
        }
        public int getTotal()
        {
            return this.total;
        }
        public void setTotal(int value)
        {
            this.total = value;
        }
        public void increaseTotal()
        {
            this.total = this.total + 1;
        }
        public void decreaseTotal()
        {
            if (this.total > 0)
            {
                this.total = this.total - 1;
            }
        }
        public void resetTotal()
        {
            this.total = 0;
        }
    }
}
